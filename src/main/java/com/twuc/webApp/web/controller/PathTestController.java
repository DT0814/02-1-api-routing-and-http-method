package com.twuc.webApp.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api")
public class PathTestController {

    //@GetMapping("/api/users/{userID}/books") 失败大小写敏感
    @GetMapping("/users/{userId}/books")
    public String books(@PathVariable(name = "userId") String userId) {
        return "Body The book for user " + userId;
    }

    @GetMapping("/segments/good")
    public String segments() {
        return "this is good";
    }

    @GetMapping("/segments/{good}")
    public String segmentsTesgt(@PathVariable(name = "good") String good) {
        return "this is not good";
    }

    @GetMapping("/question/?")
    public String question() {
        return "is ok";
    }

    @GetMapping("/wildcards/*")
    public String wildcardsAny() {
        return "*";
    }

    @GetMapping("/wildcards/before/*/after")
    public String beforeAfter() {
        return "before*after";
    }

    @GetMapping("/*test")
    public String test() {
        return "*test";
    }

    @GetMapping("/test*")
    public String testTwo() {
        return "test*";
    }

    @GetMapping("/**/any")
    public String doubleAny() {
        return "doubleAny";
    }

}
