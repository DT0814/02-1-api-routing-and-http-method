package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class PathTestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_path_connect() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk())
                .andExpect(content().string("Body The book for user 2"));
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(status().isOk())
                .andExpect(content().string("Body The book for user 23"));

    }

    @Test
    void test_path_segments() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(status().isOk())
                .andExpect(content().string("this is good"));
    }

    @Test
    void test_question_mark() throws Exception {
        mockMvc.perform(get("/api/question/m"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/question/"))
                .andExpect(status().isNotFound());
        mockMvc.perform(get("/api/question/mark"))
                .andExpect(status().isNotFound());
    }

    @Test
    void test_multiplication_symbol() throws Exception {
        mockMvc.perform(get("/api/wildcards/any"))
                .andExpect(status().isOk())
                .andExpect(content().string("*"));
        mockMvc.perform(get("/api/wildcards/before/abc/after"))
                .andExpect(status().isOk())
                .andExpect(content().string("before*after"));
        mockMvc.perform(get("/api/*test"))
                .andExpect(status().isOk())
                .andExpect(content().string("*test"));
        mockMvc.perform(get("/api/test*"))
                .andExpect(status().isOk())
                .andExpect(content().string("test*"));
    }

    @Test
    void test_double_multiplication_symbol() throws Exception {
        mockMvc.perform(get("/api/double/any"))
                .andExpect(status().isOk())
                .andExpect(content().string("doubleAny"));
    }
}
